import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';
  _LoginPageState createState() => _LoginPageState();
}

Future<Users> fetchUser() async {
  final response = await http.get('https://jsonplaceholder.typicode.com/Users');

  if (response.statusCode == 200) {
    return Users.fromJson(json.decode(response.body));
  } else {
    throw Exception('Falha ao buscar usuários');
  }
}

class User {
  final String id;
  final String name;
  final String username;
  final String email;
  final Map address;
  final String phone;
  final String website;
  final Map company;

  User(
      {this.id,
      this.email,
      this.username,
      this.name,
      this.address,
      this.phone,
      this.website,
      this.company});
}

class Users {
  List<User> list;

  factory Users.fromJson(Map<String, dynamic> json) {
    return list(
      id: json['id'],
      email: json['email'],
      name: json['name'],
      username: json['username'],
      address: json['address'],
      phone: json['phone'],
      website: json['website'],
      company: json['company'],
    );
  }
}

class _LoginPageState extends State<LoginPage> {
  var user = '';
  var secret = '';
  final userController = TextEditingController();
  final passwordController = TextEditingController();

  void userDispose() {
    userController.dispose();
    super.dispose();
  }

  void passwordDispose() {
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('assets/logo.png'),
      ),
    );

    final email = TextFormField(
      controller: userController,
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
          hintText: 'Email',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final password = TextFormField(
      autofocus: false,
      controller: passwordController,
      obscureText: true,
      decoration: InputDecoration(
          hintText: 'password',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.lightBlueAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
            minWidth: 200.0,
            height: 42.0,
            onPressed: () {
              user = userController.text;
              secret = passwordController.text;
              child:
              FutureBuilder<User>(
                  future: fetchUser(),
                  builder: (context, snapshot) {
                    print(snapshot.data.email);
                    print(snapshot.data.name);
                    //if (snapshot.data.email == user && snapshot.data.secret == secret){
                    // Navigator.of(context).pushNamed(HomePage.tag);
                    //}
                  });
            },
            color: Colors.lightBlueAccent,
            child: Text(
              'Login',
              style: TextStyle(color: Colors.white),
            )),
      ),
    );

    final forgotLabel = FlatButton(
      child: Text(
        'Forgot Password?',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {},
    );
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
          child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.only(left: 24.0, right: 24.0),
        children: <Widget>[
          logo,
          SizedBox(
            height: 48.0,
          ),
          email,
          SizedBox(
            height: 8.0,
          ),
          password,
          SizedBox(
            height: 24.0,
          ),
          loginButton,
          forgotLabel,
        ],
      )),
    );
  }
}
