import 'package:flutter/material.dart';
import 'pages/login.page.dart';
import 'pages/home.page.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget{
  final routes = <String, WidgetBuilder>{
    LoginPage.tag: (context)=>LoginPage(),
    HomePage.tag: (context)=>HomePage(),
  };
  
  @override
  Widget build(BuildContext context){
    return new MaterialApp(
      title: 'login test',
      debugShowCheckedModeBanner: true,
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
        fontFamily: 'Nunito',
      ),
      home: new LoginPage(),
      routes: routes,
    );
  }
}

